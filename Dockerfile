FROM node:lts-alpine

WORKDIR /app
RUN chown -R node:node /app
USER node

COPY package.json yarn.lock /app/
RUN yarn

COPY . /app/
RUN yarn build

CMD [ "node", "./build/src/index.js" ]
