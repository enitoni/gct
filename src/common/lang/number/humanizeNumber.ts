const units = ["", "k", "M", "B", "T", "?"]

export const humanizeNumber = (n: number) => {
  let unit = 0
  let number = n

  while (number >= 1000 && unit < units.length - 1) {
    unit += 1
    number /= 1000
  }

  const formattedNumber = number.toLocaleString("en-US", {
    maximumFractionDigits: 2
  })

  return `${formattedNumber}${units[unit]}`
}
