export const clamp = (v: number, max = 1, min = 0) =>
  Math.min(Math.max(v, min), max)
