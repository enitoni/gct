export class Lock {
  private queue: ((v?: unknown) => void)[] = []
  private locked = false

  public async acquire() {
    if (!this.locked) {
      this.locked = true
      return
    }

    return new Promise((resolve) => {
      this.queue.push(resolve)
    })
  }

  public release() {
    const next = this.queue.shift()

    if (next) {
      return next()
    }

    this.locked = false
  }
}
