export const resolveArray = <T>(v: T | T[]) => (Array.isArray(v) ? v : [v])
