export const getHumanizedJoin = (arr: string[]) =>
  arr.reduce((current, next, index, { length }) =>
    index + 1 === length ? `${current}, and ${next}` : `${current}, ${next}`
  )
