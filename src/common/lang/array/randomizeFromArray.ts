export const randomizeFromArray = <T>(arr: T[], length: number) => {
  let list: T[] = []

  for (let i = 0; i < length; i++) {
    list.push(arr[Math.floor(Math.random() * arr.length)])
  }

  return list
}
