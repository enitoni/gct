export const range = (r: number) =>
  new Array(r).fill(undefined).map((_, i) => i)
