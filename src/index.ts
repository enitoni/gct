import { Adapter, Bot } from "@enitoni/gears-discordjs"
import { coreGroup } from "./modules/core/groups/coreGroup"
import { TranslateService } from "./modules/translate/services/TranslateService.js"
import { ServerTranslatorService } from "./modules/translate/services/ServerTranslatorService.js"
import { LoggingService } from "./modules/admin/services/LoggingService.js"
import { MoodService } from "./modules/mood/services/MoodService.js"
import { CoronavirusService } from "./modules/c19/services/CoronavirusService.js"
import { RateLimitService } from "./modules/utility/services/RateLimitService.js"
import { IgnoreService } from "./modules/admin/services/IgnoreService.js"
import { PrefixService } from "./modules/admin/services/PrefixService"
import { HangmanService } from "./modules/hangman/services/HangmanService"
import { Intents } from "discord.js"
import { config } from "./modules/core/config"
import { DataSessionService } from "./modules/ml/services/DataSessionService"

const intents = new Intents(Intents.NON_PRIVILEGED)

intents.add(Intents.FLAGS.GUILD_MEMBERS)
intents.remove(
  Intents.FLAGS.GUILD_MESSAGE_TYPING,
  Intents.FLAGS.DIRECT_MESSAGE_TYPING
)

const adapter = new Adapter({
  token: config.token,
  ws: { intents },
})

const bot = new Bot({
  adapter,
  commands: [coreGroup],
  services: [
    TranslateService,
    RateLimitService,
    ServerTranslatorService,
    DataSessionService,
    CoronavirusService,
    HangmanService,
    LoggingService,
    IgnoreService,
    PrefixService,
    MoodService,
  ],
})


process.on("SIGINT", () => {
  bot.client.destroy()
})

process.on("SIGTERM", () => {
  bot.client.destroy()
})

async function main() {
  await bot.start()

  console.log("Big gay")
}

main()
