import { Service } from "@enitoni/gears-discordjs"
import * as cheerio from "cheerio"
import axios from "axios"
import { C19_SCRAPE_URL } from "../constants"
import { CoronavirusStats } from "../types/CoronavirusStats"

const parseNumber = (str: string) => Number(str.replace(/\,/g, ""))

const defaultData: CoronavirusStats = {
  global: {
    recovered: 0,
    cases: 0,
    dead: 0,
  },
  locations: [],
}

const fiveMinutes = 60000 * 5

export class CoronavirusService extends Service {
  public data = defaultData

  public async serviceDidInitialize() {
    try {
      await this.update()
    } catch {}

    global.setInterval(() => {
      this.update()
    }, fiveMinutes)
  }

  public async update() {
    const { data: html } = await axios.get(C19_SCRAPE_URL)
    const $ = cheerio.load(html)

    const [cases, dead, recovered] = $(".maincounter-number > span")
      .get()
      .map((el: CheerioElement) => parseNumber($(el).text()))

    if ([cases, dead, recovered].some((x) => typeof x === "undefined")) {
      throw new Error("Failed to scrape.")
    }

    const locations = $("#main_table_countries_today tbody:nth-of-type(1) tr")
      .get()
      .map((el: Cheerio) => {
        return $(el)
          .children("td")
          .get()
          .map((el: CheerioElement) => $(el).text())
      })
      .map((row) => {
        const [, name, cases, _, dead, __, recovered] = row

        return {
          name: name.trim(),
          cases: parseNumber(cases),
          dead: parseNumber(dead),
          recovered: parseNumber(recovered),
        }
      })

    if (locations.length === 0) {
      throw new Error("Failed to scrape countries")
    }

    this.data = {
      global: { cases, dead, recovered },
      locations,
    }
  }
}
