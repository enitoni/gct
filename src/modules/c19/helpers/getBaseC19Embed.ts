import { MessageEmbed } from "discord.js"

export const getBaseC19Embed = () =>
  new MessageEmbed({
    color: 0xfd745a,
    author: {
      name: "COVID-19",
    },
    footer: {
      text: "www.worldometers.info",
      iconURL: "https://www.worldometers.info/favicon/favicon-32x32.png",
    },
  })
