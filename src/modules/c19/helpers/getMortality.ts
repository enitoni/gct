import { StatEntry } from "../types/CoronavirusStats"

export const getMortality = (stat: StatEntry) =>
  ((100 / (stat.recovered + stat.dead)) * stat.dead).toFixed(2)
