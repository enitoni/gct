import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { globalStatCommand } from "../commands/globalStatCommand"
import { locationStatCommand } from "../commands/locationStatCommand"
import { createMetadata } from "../../core/helpers/createMetadata"
import { topLocationsCommand } from "../commands/topLocationsCommand"
import { refreshCommand } from "../commands/refreshCommand"

export const c19Group = new CommandGroup()
  .match(matchPrefixes("c19", "coronavirus"))
  .setMetadata(
    createMetadata({
      name: "Coronavirus group",
      description: "Get statistics and information about the coronavirus.",
      usage: "(!c19, !coronavirus) <country>/top"
    })
  )
  .setCommands(
    topLocationsCommand,
    refreshCommand,
    locationStatCommand,
    globalStatCommand
  )
