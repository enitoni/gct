import { Command } from "@enitoni/gears-discordjs"
import { matchAlways } from "@enitoni/gears"
import { CoronavirusService } from "../services/CoronavirusService"
import { getBaseC19Embed } from "../helpers/getBaseC19Embed"
import { humanizeNumber } from "../../../common/lang/number/humanizeNumber"
import { getMortality } from "../helpers/getMortality"

export const globalStatCommand = new Command()
  .match(matchAlways())
  .use(context => {
    const { message, manager } = context
    const service = manager.getService(CoronavirusService)
    const { cases, dead, recovered } = service.data.global

    const embed = getBaseC19Embed()
      .setTitle("Global statistics")
      .setDescription(
        "Here are the known global statistics so far. Updates every day at GMT+0"
      )
      .addField("Total cases", humanizeNumber(cases), true)
      .addField("Total dead", humanizeNumber(dead), true)
      .addField("Total recovered", humanizeNumber(recovered), true)
      .addField("Mortality rate", `${getMortality(service.data.global)}%`)

    return message.channel.send(embed)
  })
