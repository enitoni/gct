import { Command, Matcher } from "@enitoni/gears-discordjs"
import { CoronavirusService } from "../services/CoronavirusService"
import { getBaseC19Embed } from "../helpers/getBaseC19Embed"
import { matchPrefixes } from "@enitoni/gears"
import { LocationStatEntry } from "../types/CoronavirusStats"
import { humanizeNumber } from "../../../common/lang/number/humanizeNumber"
import { CommandError } from "../../core/classes/CommandError"
import { C19_SCRAPE_URL } from "../constants"
import { OWNER_USER_ID } from "../../admin/constants"

export const refreshCommand = new Command()
  .match(matchPrefixes("refresh"))
  .use(async context => {
    const { message, manager } = context
    const service = manager.getService(CoronavirusService)

    try {
      await service.update()
    } catch {
      throw new CommandError(
        "unknown",
        `Failed to scrape data from site. Make sure ${C19_SCRAPE_URL} is working, if it is contact <@${OWNER_USER_ID}> and tell him scraping broke.`
      )
    }

    return message.channel.send("Refreshed data from site.")
  })
