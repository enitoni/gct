import { Command, Matcher } from "@enitoni/gears-discordjs"
import { CoronavirusService } from "../services/CoronavirusService"
import { getBaseC19Embed } from "../helpers/getBaseC19Embed"
import { matchPrefixes } from "@enitoni/gears"
import { LocationStatEntry } from "../types/CoronavirusStats"
import { humanizeNumber } from "../../../common/lang/number/humanizeNumber"

export const topLocationsCommand = new Command()
  .match(matchPrefixes("top"))
  .use(context => {
    const { message, manager } = context

    const service = manager.getService(CoronavirusService)
    const locations = [...service.data.locations]
      .sort((a, b) => b.cases - a.cases)
      .slice(0, 25)

    const embed = getBaseC19Embed()
      .setTitle(`Top 25`)
      .setDescription(
        "Here you can see the top 25 locations based on confirmed cases."
      )
      .addField(
        "Locations",
        locations
          .map(
            (location, i) =>
              `${i + 1}. ${location.name} - ${humanizeNumber(location.cases)}`
          )
          .join("\n")
      )

    return message.channel.send(embed)
  })
