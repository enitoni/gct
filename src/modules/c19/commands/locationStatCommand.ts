import { Command, Matcher } from "@enitoni/gears-discordjs"
import { CoronavirusService } from "../services/CoronavirusService"
import { getBaseC19Embed } from "../helpers/getBaseC19Embed"
import { humanizeNumber } from "../../../common/lang/number/humanizeNumber"
import { CommandError } from "../../core/classes/CommandError"
import { getMortality } from "../helpers/getMortality"

const matchHasContent: Matcher = context =>
  context.content ? context : undefined

export const locationStatCommand = new Command()
  .match(matchHasContent)
  .use(context => {
    const { message, content, manager } = context

    const service = manager.getService(CoronavirusService)
    const location = service.data.locations.find(location =>
      location.name.toLowerCase().includes(content.toLowerCase())
    )

    if (!location) {
      throw new CommandError(
        "usage",
        "The specified location was not found. Try a country name or Diamond Princess."
      )
    }

    const { cases, dead, recovered } = location
    const samples = dead + recovered

    const embed = getBaseC19Embed()
      .setTitle(`Statistics for ${location.name}`)
      .addField("Confirmed cases", humanizeNumber(cases), true)
      .addField("Dead", humanizeNumber(dead), true)
      .addField("Recovered", humanizeNumber(recovered), true)

    if (samples >= 35) {
      embed.addField("Mortality rate", `${getMortality(location)}%`)
    }

    if (samples < 35) {
      embed.setDescription(
        `Mortality rate is not shown here because the sample size (${samples}) is too low to be significant.`
      )
    }

    if (samples > 35 && samples < 100) {
      embed.setDescription(
        `Mortality rate may be very innacurate due to sample size (${samples}) being lower than 100.`
      )
    }

    if (samples >= 100) {
      embed.setDescription(
        `Here you can see statistics for ${location.name}.\n
        The mortality rate is calculated based on the amount of resolved cases and the death count.`
      )
    }

    return message.channel.send(embed)
  })
