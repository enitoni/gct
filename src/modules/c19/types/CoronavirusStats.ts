export type StatEntry = {
  recovered: number
  cases: number
  dead: number
}

export type LocationStatEntry = StatEntry & {
  name: string
}

export type CoronavirusStats = {
  global: StatEntry
  locations: LocationStatEntry[]
}
