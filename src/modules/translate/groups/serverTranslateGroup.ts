import { CommandGroup, Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { requireOwner } from "../../admin/middleware/requireOwner"
import { ServerTranslatorService } from "../services/ServerTranslatorService"

const translate = new Command()
  .match(matchPrefixes("translate", "t"))
  .use(async (context) => {
    const { message, manager } = context

    await message.channel.send("Hold on...")
    message.channel.startTyping()

    const service = manager.getService(ServerTranslatorService)
    await service.translateServer()

    message.channel.stopTyping()
    return message.channel.send("Done! Enjoy your translations...")
  })

const snapshot = new Command()
  .match(matchPrefixes("snapshot", "s"))
  .use(async (context) => {
    const { message, manager } = context

    const service = manager.getService(ServerTranslatorService)
    await service.takeSnapshot()

    return message.channel.send(
      "OK, I'll remember what your server originally looked like."
    )
  })

const restore = new Command()
  .match(matchPrefixes("restore", "r"))
  .use(async (context) => {
    const { message, manager } = context

    await message.channel.send("One sec...")
    message.channel.startTyping()

    const service = manager.getService(ServerTranslatorService)
    await service.restoreServer()

    message.channel.stopTyping()
    return message.channel.send("Done! The server has been restored.")
  })

export const serverTranslateGroup = new CommandGroup()
  .match(matchPrefixes("st "))
  .use(requireOwner())
  .setCommands(translate, snapshot, restore)
