import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchAlways } from "@enitoni/gears"
import { badTranslateCommand } from "../commands/badTranslateCommand"
import { translateHistorycommand } from "../commands/translateHistoryCommand"
import { translateStatsCommand } from "../commands/translateStatsCommand"
import { bigBrainCommand } from "../commands/bigBrainCommand"
import { translateCommand } from "../commands/translateCommand"
import { setTranslateConfigCommand } from "../commands/setTranslateConfigCommand"

export const translateGroup = new CommandGroup()
  .match(matchAlways())
  .setCommands(
    setTranslateConfigCommand,
    badTranslateCommand,
    translateHistorycommand,
    translateStatsCommand,
    translateCommand,
    bigBrainCommand
  )
