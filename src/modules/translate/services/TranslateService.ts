import { Service } from "@enitoni/gears-discordjs"
import { TranslationStats } from "../classes/TranslationStats.js"
import { DEFAULT_LANGUAGE, PER_TRANSLATION_CHARACTER_LIMIT } from "../constants.js"
import { CommandError } from "../../core/classes/CommandError.js"
import { randomizeFromArray } from "../../../common/lang/array/randomizeFromArray.js"
import { humanizeNumber } from "../../../common/lang/number/humanizeNumber.js"
import { JSONStorage } from "../../../common/storage/classes/JSONStorage.js"
import { TranslationClient } from "../classes/TranslationClient.js"
import { microsoftTranslator } from "../clients/microsoftTranslator.js"
import { paidGoogleTranslator } from "../clients/paidGoogleTranslator.js"

const stats = new TranslationStats()

export type TranslateConfig = {
  target: string
}

export type TranslateResult = {
  lang: string
  content: string
}

export type TranslateModifier = (lang: string, content: string) => string

const defaultConfig: TranslateConfig = {
  target: DEFAULT_LANGUAGE,
}

const defaultModifier: TranslateModifier = (_, c) => c
const storage = new JSONStorage<Record<string, TranslateConfig>>(
  "translate-config.json",
  {}
)

export class TranslateService extends Service {
  private currentClient!: TranslationClient
  private clients: TranslationClient[] = []

  public randomHistory: TranslateResult[] = []

  public async serviceDidInitialize() {
    this.clients = [await microsoftTranslator, await paidGoogleTranslator]

    await stats.restore()
    await storage.restore()

    this.ensureCorrectStats()
    this.ensureCorrectClient()
  }

  private ensureCorrectClient() {
    const { current } = stats.data

    const maxLimit = this.clients.reduce(
      (acc, x) => acc + x.limits.maxCharacters,
      0
    )

    let total = 0

    for (const client of this.clients) {
      total += client.limits.maxCharacters

      if (Math.min(current.characters, maxLimit) <= total) {
        this.currentClient = client
        return
      }
    }

    throw new Error("Couldn't find a translation client!")
  }

  public async setConfig(id: string, config: TranslateConfig) {
    await storage.save({ ...storage.data, [id]: config })
  }

  public getConfig(id: string) {
    return storage.data[id] ?? defaultConfig
  }

  public getLangCodeByName(name: string) {
    const lang = this.client.languages.find(([n]) =>
      n.toLowerCase().includes(name.toLowerCase())
    )

    if (!lang) {
      throw new CommandError(
        "usage",
        `The current provider (${this.client.name}) does not support ${name}.`
      )
    }

    return lang[1]
  }

  public async translateToLanguages(
    languages: string[],
    content: string,
    modify = defaultModifier
  ) {
    await this.ensureTranslateLimit()

    const codes = languages.map((x) => this.getLangCodeByName(x))

    let result: TranslateResult = {
      content,
      lang: "auto",
    }

    const history: TranslateResult[] = [result]

    for (const [i, lang] of codes.entries()) {
      const newContent = await this.client.translate(
        result.content,
        result.lang,
        lang
      )

      if (newContent && newContent.length <= PER_TRANSLATION_CHARACTER_LIMIT) {
        const safeContent = (
          i === languages.length - 1 ? newContent : modify(lang, newContent)
        ).replace(/(.)\1{2,}/g, "")

        const newResult = {
          lang: lang,
          content: safeContent,
        }

        result = newResult
        history.push(newResult)
      }
    }

    this.randomHistory = history

    await this.collectStatsFromHistory()
    this.ensureCorrectClient()

    return result.content
  }

  public async translateToRandomLanguages(
    content: string,
    target: string,
    count = 5
  ) {
    const langs = [
      ...randomizeFromArray(this.languagesByName, count).filter(
        (x) => x !== "English"
      ),
      target,
    ]

    const result = await this.translateToLanguages(langs, content)

    return result
  }

  private async collectStatsFromHistory() {
    const translations = this.randomHistory.length - 1
    const characters = this.randomHistory.reduce(
      (acc, entry) => acc + entry.content.length,
      0
    )

    await stats.add(translations, characters)
  }

  private async ensureTranslateLimit() {
    await stats.ensureCorrectMonth()

    const { current } = stats.data

    if (current.characters >= this.quota) {
      throw new CommandError(
        "usage",
        `Translation quota of ${humanizeNumber(
          this.quota
        )} exceeded. Wait until next month.`
      )
    }
  }

  public get client() {
    return this.currentClient
  }

  public get languagesByName() {
    return this.currentClient.languages.map(([name]) => name)
  }

  public get quota() {
    return this.clients.reduce((acc, c) => acc + c.limits.maxCharacters, 0)
  }

  public get stats() {
    return stats.data
  }

  public ensureCorrectStats() {
    return stats.ensureCorrectMonth()
  }
}
