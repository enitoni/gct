import { Service } from "@enitoni/gears-discordjs"
import { GuildMember, TextChannel, Guild } from "discord.js"
import { TranslateService } from "./TranslateService"
import { PERSONAL_SERVER_ID } from "../../core/constants"
import { JSONStorage } from "../../../common/storage/classes/JSONStorage"
import { wait } from "../../../common/lang/promise/wait"

export type TranslatedServerSnapshot = {
  restored: boolean
  name: string
  channels: {
    id: string
    name: string
    topic: string
  }[]
  roles: {
    id: string
    name: string
  }[]
}

const storage = new JSONStorage<TranslatedServerSnapshot>(
  "translated-server-snapshot.json",
  {
    restored: false,
    name: "",
    channels: [],
    roles: [],
  }
)

const WAIT_MS = 500
const TARGET_LANGUAGE = "English"

export class ServerTranslatorService extends Service {
  private guild!: Guild

  public async serviceDidInitialize() {
    const { bot } = this

    bot.client.on("guildMemberAdd", this.handleNewMember)
    bot.client.on("guildMemberUpdate", this.handleMemberUpdate)

    await storage.restore()
  }

  public async serviceDidStart() {
    const { client } = this.bot

    this.guild = client.guilds.cache.find((g) => g.id === PERSONAL_SERVER_ID)!
  }

  private handleNewMember = async (member: GuildMember) => {
    const service = this.manager.getService(TranslateService)
    const { guild } = member

    if (guild.id !== PERSONAL_SERVER_ID) return

    const nickname = await service.translateToRandomLanguages(
      member.displayName,
      TARGET_LANGUAGE,
      8
    )

    await member.setNickname(nickname.slice(0, 32))
    await member.user.send(
      `Welcome to the google translate server, ${nickname}!`
    )
  }

  private handleMemberUpdate = async (_: any, member: GuildMember) => {
    const service = this.manager.getService(TranslateService)
    const { guild } = member

    const hasNick =
      !!member.nickname && member.nickname !== member.user.username

    if (guild.id !== PERSONAL_SERVER_ID || hasNick) {
      return
    }

    const nickname = await service.translateToRandomLanguages(
      member.displayName,
      TARGET_LANGUAGE,
      8
    )

    await member.setNickname(nickname.slice(0, 32))
    await member.user.send(
      `I noticed you removed your nickname and I will assume this means you want a brand new one, so now your name is ${nickname}!`
    )
  }

  /**
   * Takes a snapshot of the original server before translations
   */
  public async takeSnapshot() {
    const { restored } = storage.data

    if (!restored) {
      throw new Error("Cannot take snapshot before server is restored")
    }

    const serializedChannels = this.guild.channels.cache.map((channel) => ({
      id: channel.id,
      name: channel.name,
      topic: (channel as TextChannel).topic || "",
    }))

    const serializedRoles = this.guild.roles.cache.map((role) => ({
      id: role.id,
      name: role.name,
    }))

    await storage.save({
      restored: true,
      name: this.guild.name,
      channels: serializedChannels,
      roles: serializedRoles,
    })
  }

  /**
   * Translates channels and name randomly
   */
  public async translateServer() {
    const { manager, roles, channels, guild } = this
    const { name } = storage.data

    const service = manager.getService(TranslateService)

    const newName = await service.translateToRandomLanguages(
      name,
      TARGET_LANGUAGE,
      10
    )
    await guild.edit({ name: newName })

    for (const entry of roles) {
      const { instance, name } = entry

      const newName = await service.translateToRandomLanguages(
        name,
        TARGET_LANGUAGE,
        8
      )
      await instance.edit({ name: newName })
      await wait(WAIT_MS)
    }

    for (const entry of channels) {
      const { instance, name, topic } = entry

      if (instance instanceof TextChannel) {
        const disclaimer = `#${name}`

        const translated = topic
          ? `${disclaimer} - ${await service.translateToRandomLanguages(
              topic,
              TARGET_LANGUAGE,
              8
            )}`
          : disclaimer

        await instance.edit({ topic: translated })
      }

      const newName = await service.translateToRandomLanguages(
        entry.name,
        TARGET_LANGUAGE,
        8
      )
      await instance.edit({ name: newName })
      await wait(WAIT_MS)
    }

    await storage.save({ ...storage.data, restored: false })
  }

  public async restoreServer() {
    const { guild, roles, channels } = this
    const { name } = storage.data

    await guild.edit({ name })

    for (const entry of roles) {
      const { instance, name } = entry
      await instance.edit({ name })
      await wait(WAIT_MS)
    }

    for (const entry of channels) {
      const { instance, name, topic } = entry
      await instance.edit({ name })

      if (instance instanceof TextChannel) {
        await instance.edit({ topic })
      }

      await wait(WAIT_MS)
    }

    await storage.save({ ...storage.data, restored: true })
  }

  public get channels() {
    return this.guild.channels.cache.map((c) => ({
      instance: c,
      ...(storage.data.channels.find((x) => x.id === c.id) || {
        id: c.id,
        name: c.name,
        topic: c instanceof TextChannel ? c.topic ?? "" : "",
      }),
    }))
  }

  public get roles() {
    return this.guild.roles.cache.map((r) => ({
      instance: r,
      ...(storage.data.roles.find((x) => x.id === r.id) || {
        id: r.id,
        name: r.name,
      }),
    }))
  }
}
