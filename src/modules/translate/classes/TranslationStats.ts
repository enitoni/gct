import { JSONStorage } from "../../../common/storage/classes/JSONStorage"

export type TranslateMonthStats = {
  month: number
  translations: number
  characters: number
}

export type TranslateStats = {
  current: TranslateMonthStats
  last: TranslateMonthStats
  totalTranslations: number
  totalCharacters: number
}

const getDefaultMonthStats = (): TranslateMonthStats => ({
  month: new Date().getMonth(),
  translations: 0,
  characters: 0
})

const storage = new JSONStorage<TranslateStats>("translate-stats.json", {
  current: getDefaultMonthStats(),
  last: getDefaultMonthStats(),
  totalTranslations: 0,
  totalCharacters: 0
})

export class TranslationStats {
  public restore() {
    return storage.restore()
  }

  public get data() {
    return storage.data
  }

  /** Checks that the current month is the same as the current month stored */
  public async ensureCorrectMonth() {
    const { current } = storage.data

    const now = new Date()
    const month = now.getMonth()

    if (month === current.month) {
      return
    }

    await storage.save({
      ...storage.data,
      current: getDefaultMonthStats(),
      last: current
    })
  }

  public async add(translations: number, characters: number) {
    await this.ensureCorrectMonth()

    const { totalCharacters, totalTranslations, current } = storage.data

    await storage.save({
      ...storage.data,
      current: {
        ...current,
        characters: current.characters + characters,
        translations: current.translations + translations
      },
      totalCharacters: totalCharacters + characters,
      totalTranslations: totalTranslations + translations
    })
  }
}
