import { Lock } from "../../../common/lang/promise/classes/Lock"
import { wait } from "../../../common/lang/promise/wait"

export type TranslationClientOptions = {
  name: string
  limits: {
    maxCharacters: number
    minMsPerRequest: number
  }
  translate: (input: string, from: string, to: string) => Promise<string>
  languages: () => Promise<[string, string][]>
}

export class TranslationClient {
  private lock = new Lock()
  public languages: [string, string][] = []

  private constructor(private options: TranslationClientOptions) {}

  public static async create(options: TranslationClientOptions) {
    const instance = new TranslationClient(options)
    await instance.fetchLanguages()

    return instance
  }

  public async translate(input: string, from: string, to: string) {
    await this.lock.acquire()

    try {
      const result = await this.options.translate(input, from, to)
      this.release()

      return result
    } catch (e) {
      this.release()
      throw e
    }
  }

  private release() {
    setTimeout(() => this.lock.release(), this.limits.minMsPerRequest)
  }

  private async fetchLanguages() {
    this.languages = await this.options.languages()
  }

  public get name() {
    return this.options.name
  }

  public get limits() {
    return this.options.limits
  }
}
