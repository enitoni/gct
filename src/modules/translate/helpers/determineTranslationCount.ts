export type DetermineTranslationCountOptions = {
  length: number
  steepness: number
  limit: number
  min: number
  max: number
}

export const determineTranslationCount = (
  options: DetermineTranslationCountOptions
) =>
  Math.round(
    options.min +
      (options.max - options.min) *
        (1 - Math.pow(options.length / options.limit, options.steepness))
  )
