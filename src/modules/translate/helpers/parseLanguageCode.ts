import { CommandError } from "../../core/classes/CommandError"
import { Parser } from "../../core/middleware/parseModifiers"
import { SELECTABLE_LANGUAGES } from "../constants"

export const parseLanguageCode: Parser<string> = (label, value) => {
  const normalizedSelectableLanguages = SELECTABLE_LANGUAGES.map((x) =>
    x.toLowerCase()
  )

  if (normalizedSelectableLanguages.includes(value.toLowerCase())) {
    return value
  }

  throw new CommandError(
    "usage",
    `${label} must be one of: ${SELECTABLE_LANGUAGES.join(", ")}`
  )
}
