import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import * as japanese from "../../../../japanese.json"
import { getPrefixIdentifier } from "../../admin/helpers/getPrefixIdentifier"
import { createMetadata } from "../../core/helpers/createMetadata.js"
import {
  parseModifiers,
  parseNumber
} from "../../core/middleware/parseModifiers"
import { ratelimit } from "../../utility/middleware/ratelimit.js"
import { parseLanguageCode } from "../helpers/parseLanguageCode"
import { TranslateService } from "../services/TranslateService.js"

const langs = [
  "Arabic",
  "Hebrew",
  "German",
  "Hindi",
  "Chinese",
  "Thai",
  "Korean",
  "Czech",
  "English",
  "Spanish",
  "Russian",
  "Norwegian",
]

const lexemes = [
  "to",
  "if",
  "can",
  "for",
  "the",
  "not",
  "with",
  "must",
  "doing",
  "while",
  "should",
]

const min = 10
const max = 40

const weights = ["kanji", "kanji", "hiragana"] as const

export const bigBrainCommand = new Command()
  .match(matchPrefixes("bigbrain", "bb"))
  .setMetadata(
    createMetadata({
      name: "Big brain",
      usage: `(!bigbrain,!bb)[.${min}-${max}.language]`,
      description: `Tries to create a sentence out of thin air and translates it. Default word count is ${min}.`,
    })
  )
  .use(ratelimit("translate", 3))
  .use(
    parseModifiers([
      {
        name: "length",
        label: "Length",
        default: 10,
        parse: parseNumber(min, max),
      },
      {
        name: "language",
        label: "Language code",
        default: "DEFAULT",
        parse: parseLanguageCode,
      } as const,
    ])
  )
  .use(async (context) => {
    const { message, manager, state } = context
    const { length, language } = state.modifiers

    const service = manager.getService(TranslateService)
    const config = service.getConfig(getPrefixIdentifier(message))

    const targetLanguage = language.replace("DEFAULT", config.target)

    let content = ""
    let lastType = ""

    while (content.length < length) {
      const filteredWeights = weights.filter((type) => lastType !== type)
      const type =
        filteredWeights[Math.floor(Math.random() * filteredWeights.length)]
      lastType = type

      const array = japanese[type]
      const chars = Math.ceil(Math.random() * 3)

      for (let i = 0; i < chars; i++) {
        content += array[Math.floor(Math.random() * array.length)]
      }
    }

    message.channel.startTyping()
    const result = await service.translateToLanguages(
      [...langs, targetLanguage],
      content,
      (lang, content) => {
        if (lang === "en") {
          return content.replace(
            /,/g,
            () => " " + lexemes[Math.floor(Math.random() * lexemes.length)]
          )
        }

        return content
      }
    )

    message.channel.stopTyping(true)
    return message.channel.send(result)
  })
