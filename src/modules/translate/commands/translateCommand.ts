import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import { CommandError } from "../../core/classes/CommandError"
import { createMetadata } from "../../core/helpers/createMetadata"
import { ratelimit } from "../../utility/middleware/ratelimit"
import { PER_TRANSLATION_CHARACTER_LIMIT } from "../constants"
import { TranslateService } from "../services/TranslateService"

export const translateCommand = new Command()
  .match(matchPrefixes("translate", "t"))
  .setMetadata(
    createMetadata({
      name: "Translate",
      description: "Translate some text to english.",
      usage: "!t <text>",
    })
  )
  .use(ratelimit("translate", 2))
  .use(async (context) => {
    const { manager, content, message } = context
    const service = manager.getService(TranslateService)

    if (content.length > PER_TRANSLATION_CHARACTER_LIMIT) {
      throw new CommandError(
        "usage",
        `Character count exceeds ${PER_TRANSLATION_CHARACTER_LIMIT}.`
      )
    }

    message.channel.startTyping()
    const result = await service.translateToLanguages(["English"], content)
    message.channel.stopTyping(true)

    return message.channel.send(result)
  })
