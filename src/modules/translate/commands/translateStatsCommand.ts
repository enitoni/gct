import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { createMetadata } from "../../core/helpers/createMetadata"
import { TranslateService } from "../services/TranslateService"
import { MessageEmbed } from "discord.js"
import { TranslateMonthStats } from "../classes/TranslationStats"
import { humanizeNumber } from "../../../common/lang/number/humanizeNumber"

const createMonthField = (
  stats: TranslateMonthStats,
  title: string,
  quota: number
) => {
  const { characters, translations } = stats

  const percentage = Math.floor((stats.characters / quota) * 100)

  return {
    name: `${title} - ${percentage}%`,
    value:
      `Translations: ${humanizeNumber(translations)}` +
      `\nCharacters: ${humanizeNumber(characters)}`,
  }
}

export const translateStatsCommand = new Command()
  .match(matchPrefixes("ts", "translate-stats"))
  .setMetadata(
    createMetadata({
      name: "Translation stats",
      usage: "!ts",
      description: "Shows statistics for translations done through the bot.",
    })
  )
  .use((context) => {
    const { message, manager } = context
    const { stats, quota, client } = manager.getService(TranslateService)
    const { last, current, totalTranslations, totalCharacters } = stats

    const embed = new MessageEmbed({
      title: "Translation stats",
      description:
        `Total translations: ${humanizeNumber(totalTranslations)}` +
        `\nTotal characters: ${humanizeNumber(totalCharacters)}`,
      fields: [
        createMonthField(current, "This month", quota),
        createMonthField(last, "Last month", quota),
        {
          name: "Current provider",
          value: `${client.name} (${humanizeNumber(
            client.limits.maxCharacters
          )})`,
        },
      ],
    })

    return message.channel.send(embed)
  })
