import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import { getPrefixIdentifier } from "../../admin/helpers/getPrefixIdentifier"
import { requireOwner } from "../../admin/middleware/requireOwner"
import { CommandError } from "../../core/classes/CommandError"
import { SELECTABLE_LANGUAGES } from "../constants"
import { TranslateService } from "../services/TranslateService"

export const setTranslateConfigCommand = new Command()
  .match(matchPrefixes("set-translate-config ", "stc "))
  .use(requireOwner())
  .use(async (context) => {
    const { manager, message, content } = context
    const service = manager.getService(TranslateService)

    const normalizedSelectableLanguages = SELECTABLE_LANGUAGES.map((x) =>
      x.toLocaleLowerCase()
    )

    if (!normalizedSelectableLanguages.includes(content.toLowerCase())) {
      throw new CommandError(
        "usage",
        `Target language must be one of: ${SELECTABLE_LANGUAGES.join(", ")}`
      )
    }

    await service.setConfig(getPrefixIdentifier(message), { target: content })

    const response = await service.translateToLanguages(
      [content],
      `I will now speak "${content}" here.`
    )

    return message.channel.send(response)
  })
