import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { TranslateService } from "../services/TranslateService"
import { createMetadata } from "../../core/helpers/createMetadata"
import { MessageAttachment } from "discord.js"

export const translateHistorycommand = new Command()
  .match(matchPrefixes("th", "translate-history"))
  .setMetadata(
    createMetadata({
      name: "Translate history",
      usage: "!th",
      description:
        "Returns the history of translations from the most recent bad translation.",
    })
  )
  .use(async (context) => {
    const { message, manager } = context
    const { randomHistory } = manager.getService(TranslateService)

    if (randomHistory.length === 0) {
      return message.channel.send("There's nothing in the history.")
    }

    const list = randomHistory
      .map((x) => `${x.lang.toUpperCase()} • ${x.content}`)
      .join("\n→ ")

    const attachment = new MessageAttachment(
      Buffer.from(list),
      "translate-history.txt"
    )

    return message.channel.send(attachment)
  })
