import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import { getPrefixIdentifier } from "../../admin/helpers/getPrefixIdentifier"
import { CommandError } from "../../core/classes/CommandError"
import { createMetadata } from "../../core/helpers/createMetadata"
import {
  parseModifiers
} from "../../core/middleware/parseModifiers"
import { temporaryCreateReplyWorkaround } from "../../fun/helpers/temporaryCreateReplyWorkaround"
import { ratelimit } from "../../utility/middleware/ratelimit"
import { PER_TRANSLATION_CHARACTER_LIMIT } from "../constants"
import { determineTranslationCount } from "../helpers/determineTranslationCount"
import { parseLanguageCode } from "../helpers/parseLanguageCode"
import { TranslateService } from "../services/TranslateService"

const min = 5
const max = 15

/*
This number determines the steepness of the amount of translations used to message length.
If this number is 1, the number of translations go linearly as message length approaches the maximum acceptable length.
If this number is less than 1, the number of translations will decelerate as message length approaches the maximum acceptable length. This means that values will quickly decline initially, but slow down.
If this number is larger than 1, the number of translations will accelerate as message length approaches the maximum acceptable length. This means that values will very slowly change, and only when it gets close to 900 does it speed up.
Generally speaking, you should keep this number between 0 and 1.
*/
const steepness = 0.5

export const badTranslateCommand = new Command()
  .match(matchPrefixes("bt", "bad-translate"))
  .setMetadata(
    createMetadata({
      name: "Bad translate",
      description: `Translates a phrase through google translate through x random languages. The amount of languages is determined by length.`,
      usage: `!bt[.language] <phrase>`,
    })
  )
  .use((context, next) => {
    if (context.content.length > PER_TRANSLATION_CHARACTER_LIMIT) {
      throw new CommandError(
        "usage",
        `Character count exceeds ${PER_TRANSLATION_CHARACTER_LIMIT}.`
      )
    }

    return next()
  })
  .use(ratelimit("translate", 5))
  .use(
    parseModifiers([
      {
        name: "language",
        label: "Language",
        default: "DEFAULT",
        parse: parseLanguageCode,
      } as const,
    ])
  )
  .use(async (context) => {
    const { state, message, manager, content } = context
    const { language } = state.modifiers

    const service = manager.getService(TranslateService)
    const config = service.getConfig(getPrefixIdentifier(message))

    message.channel.startTyping()

    const translateCount = determineTranslationCount({
      limit: PER_TRANSLATION_CHARACTER_LIMIT,
      length: content.length,
      steepness,
      min,
      max,
    })

    const result = await service.translateToRandomLanguages(
      content,
      language.replace("DEFAULT", config.target),
      translateCount
    )

    message.channel.stopTyping(true)

    return temporaryCreateReplyWorkaround(context, result, message)
  })
