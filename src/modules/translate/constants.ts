export const PER_TRANSLATION_CHARACTER_LIMIT = 1500

export const SELECTABLE_LANGUAGES = [
  "English",
  "Norwegian",
  "Portuguese",
  "Mongolian",
  "Finnish",
  "Turkish",
  "Japanese",
]

export const DEFAULT_LANGUAGE = "English"
