import Axios from "axios"
import { TranslationClient } from "../classes/TranslationClient"
import { config } from "../../core/config"

type TranslateResponse = {
  translations: {
    text: string
  }[]
}[]

type LanguageResponse = {
  translation: Record<
    string,
    {
      name: string
    }
  >
}

const instance = Axios.create({
  baseURL: config.microsoft.endpoint,
  headers: {
    "Ocp-Apim-Subscription-Key": config.microsoft.key,
    // Region is global
    //"Ocp-Apim-Subscription-Region": microsoft.region,
  },
})

export const microsoftTranslator = TranslationClient.create({
  name: "Microsoft Azure Translator",
  limits: {
    maxCharacters: 1000 * 1000 * 2,
    minMsPerRequest: 0,
  },
  translate: async (input, from, to) => {
    const params: Record<string, string> = {
      "api-version": "3.0",
      to,
    }

    if (from !== "auto") {
      params.from = from
    }

    const { data } = await instance.post<TranslateResponse>(
      "/translate",
      [{ Text: input }],
      { params }
    )

    const [result] = data
    const [translation] = result.translations

    return translation.text
  },
  languages: async () => {
    const { data } = await instance.get<LanguageResponse>("/languages", {
      params: {
        "api-version": "3.0",
      },
    })

    const { translation } = data

    return Object.entries(translation).map(([key, entry]) => [entry.name, key])
  },
})
