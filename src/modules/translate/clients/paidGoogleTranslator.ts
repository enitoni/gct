import { v2 } from "@google-cloud/translate"
import * as path from "path"
import { TranslationClient } from "../classes/TranslationClient"

const client = new v2.Translate({
  keyFilename: path.resolve(
    process.env.APP_CONFIG || process.cwd(),
    "./gcloud.json"
  ),
})

export const paidGoogleTranslator = TranslationClient.create({
  name: "Google Translate",
  limits: {
    maxCharacters: 1000 * 1000,
    minMsPerRequest: 0,
  },
  translate: async (input, _, to) => {
    const [content] = await client.translate(input, to)
    return content
  },
  languages: async () => {
    const [result] = await client.getLanguages()
    return result.map((x) => [x.name, x.code])
  },
})
