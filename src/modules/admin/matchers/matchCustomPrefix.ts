import { Matcher } from "@enitoni/gears-discordjs"
import { PrefixService } from "../services/PrefixService"
import { matchPrefixes } from "@enitoni/gears"
import { getPrefixIdentifier } from "../helpers/getPrefixIdentifier"

export const matchCustomPrefix: Matcher = (context) => {
  const { manager, message } = context

  const service = manager.getService(PrefixService)
  const prefixes = service.getPrefix(getPrefixIdentifier(message))

  return matchPrefixes(...prefixes)(context)
}
