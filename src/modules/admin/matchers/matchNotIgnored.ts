import { Matcher } from "@enitoni/gears-discordjs"
import { IgnoreService } from "../services/IgnoreService"

export const matchNotIgnored = (): Matcher => (context) => {
  const { manager, message, bot } = context
  const { author } = message
  const service = manager.getService(IgnoreService)

  if (service.check(author)) {
    return
  }

  return context
}
