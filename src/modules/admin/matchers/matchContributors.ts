import { Matcher } from "@enitoni/gears-discordjs";
import { CONTRIBUTORS } from "../constants";


export const matchContributors = (): Matcher => (context) => {
    const { message } = context

    const isContributor = CONTRIBUTORS.some(id => message.author.id === id)
    if (isContributor) return context
}