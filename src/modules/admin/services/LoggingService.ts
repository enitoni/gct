import { Service, Command } from "@enitoni/gears-discordjs"
import { Message, TextChannel, MessageEmbed, GuildChannel } from "discord.js"
import { LOGGING_CHANNEL_ID } from "../constants"
import { CommandMetadata } from "../../core/types/CommandMetadata"

export class LoggingService extends Service {
  private channel!: TextChannel

  public serviceDidStart() {
    this.bot.on("response", this.handleResponse)
    this.channel = this.bot.client.channels.cache.find(
      (x) => x.id === LOGGING_CHANNEL_ID
    ) as TextChannel
  }

  public handleResponse = (data: {
    response: Message
    command: Command
    message: Message
  }) => {
    const { response, command, message } = data

    if (command.metadata) {
      const { name } = command.metadata as CommandMetadata
      const { username } = message.author

      const embed = new MessageEmbed({
        title: `A command was ran`,
        description: message.content,
        timestamp: new Date(message.createdTimestamp),
        author: {
          name: username,
          icon_url: message.author.displayAvatarURL(),
        },
      })

      embed.addField("Response", response.content || "_ _")

      if (message.channel instanceof GuildChannel) {
        embed.addField("Server", message.channel.guild.name)
      }

      embed.addField("Channel ID", message.channel.id)
      embed.addField("Command", name)

      this.channel.send(embed)
    }
  }
}
