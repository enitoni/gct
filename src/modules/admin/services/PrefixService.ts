import { JSONStorage } from "../../../common/storage/classes/JSONStorage"
import { Service } from "@enitoni/gears-discordjs"

export const defaultPrefix = "!"
const storage = new JSONStorage<Record<string, string[]>>("prefix.json", {})

export class PrefixService extends Service {
  public async serviceDidInitialize() {
    await storage.restore()
  }

  public async setPrefix(id: string, prefixes: string[]) {
    await storage.save({ ...storage.data, [id]: prefixes })
  }

  public getPrefix(id: string) {
    return storage.data[id] ?? [defaultPrefix]
  }
}
