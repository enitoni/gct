import { Service } from "@enitoni/gears-discordjs"
import { User } from "discord.js"
import { CommandError } from "../../core/classes/CommandError"
import { JSONStorage } from "../../../common/storage/classes/JSONStorage"

const storage = new JSONStorage<string[]>("ignored-users.json", [])

export class IgnoreService extends Service {
  private users = new Set<string>()

  public async serviceDidInitialize() {
    await this.load()
  }

  private save() {
    storage.save([...this.users])
  }

  private async load() {
    await storage.restore()
    this.users = new Set(storage.data)
  }

  public ignore(user: User) {
    if (this.check(user)) {
      throw new CommandError("usage", "That user is already ignored.")
    }

    this.users.add(user.id)
    this.save()
  }

  public pardon(user: User) {
    if (!this.check(user)) {
      throw new CommandError("usage", "That user is not ignored.")
    }

    this.users.delete(user.id)
    this.save()
  }

  public check(user: User) {
    return this.users.has(user.id)
  }
}
