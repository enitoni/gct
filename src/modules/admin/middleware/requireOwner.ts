import { Middleware } from "@enitoni/gears-discordjs"
import { OWNER_USER_ID } from "../constants"
import { CommandError } from "../../core/classes/CommandError"

export const requireOwner = (): Middleware => (context, next) => {
  const { message } = context

  if (message.author.id === OWNER_USER_ID) {
    return next()
  }

  throw new CommandError(
    "permission",
    "Only the bot owner Enitoni#0001 can do this."
  )
}
