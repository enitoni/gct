import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { IgnoreService } from "../services/IgnoreService"
import { CommandError } from "../../core/classes/CommandError"

export const ignoreCommand = new Command()
  .match(matchPrefixes("ignore"))
  .use((context) => {
    const { manager, message } = context

    const user = message.mentions.users.first()
    const service = manager.getService(IgnoreService)

    if (!user) {
      throw new CommandError("usage", "Please mention a user to ignore.")
    }

    service.ignore(user)

    return message.channel.send(
      `OK. I will treat ${user.username} like they are just the wind.`
    )
  })
