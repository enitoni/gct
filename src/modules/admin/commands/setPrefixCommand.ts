import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"

import { CommandError } from "../../core/classes/CommandError"
import { PrefixService } from "../services/PrefixService"
import { getPrefixIdentifier } from "../helpers/getPrefixIdentifier"
import { getHumanizedJoin } from "../../../common/lang/array/getHumanizedJoin"

export const setPrefixCommand = new Command()
  .match(matchPrefixes("set-prefix "))
  .use(async (context) => {
    const { manager, message, content } = context
    const service = manager.getService(PrefixService)

    const newPrefixes = content.split("|")
    const isValid = newPrefixes.every((s) => s.length !== 0)

    if (!isValid) {
      throw new CommandError("usage", "Prefix must be at least one character")
    }

    await service.setPrefix(getPrefixIdentifier(message), newPrefixes)

    return message.channel.send(
      `Cool. I will now respond to ${getHumanizedJoin(
        newPrefixes.map((g) => `"${g}"`)
      )} here.`
    )
  })
