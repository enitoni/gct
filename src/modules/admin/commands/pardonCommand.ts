import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { IgnoreService } from "../services/IgnoreService"
import { CommandError } from "../../core/classes/CommandError"

export const pardonCommand = new Command()
  .match(matchPrefixes("pardon"))
  .use((context) => {
    const { manager, message } = context

    const user = message.mentions.users.first()
    const service = manager.getService(IgnoreService)

    if (!user) {
      throw new CommandError("usage", "Please mention a user to pardon.")
    }

    service.pardon(user)

    return message.channel.send(
      `OK. I will treat ${user.username} like they are someone I should listen to.`
    )
  })
