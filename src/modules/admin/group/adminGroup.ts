import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchAlways } from "@enitoni/gears"
import { ignoreCommand } from "../commands/ignoreCommand"
import { pardonCommand } from "../commands/pardonCommand"
import { requireOwner } from "../middleware/requireOwner"
import { setPrefixCommand } from "../commands/setPrefixCommand"

export const adminGroup = new CommandGroup()
  .match(matchAlways())
  .use(requireOwner())
  .setCommands(ignoreCommand, pardonCommand, setPrefixCommand)
