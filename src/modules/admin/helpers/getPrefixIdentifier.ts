import { Message } from "discord.js"

export const getPrefixIdentifier = (message: Message) =>
  message.guild ? `guild-${message.guild.id}` : `channel-${message.channel.id}`
