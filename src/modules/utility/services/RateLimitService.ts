import { Service } from "@enitoni/gears-discordjs"
import { add, isBefore } from "date-fns"
import { clamp } from "../../../common/lang/number/clamp"

export type RateLimitEntry = {
  key: string
  expire: Date
  cooldown: Date
}

export class RateLimitService extends Service {
  private userMap: Record<string, RateLimitEntry[]> = {}

  private getOrCreate(user: string) {
    const entry = this.userMap[user] || []
    this.userMap[user] = entry

    return entry
  }

  public limit(user: string, key: string, cooldown: number) {
    const entries = this.getOrCreate(user)
    const now = new Date()

    const cooldownDate = add(now, { seconds: cooldown })

    const expireDate = add(cooldownDate, {
      seconds: clamp(cooldown, 8, 30)
    })

    entries.push({
      cooldown: cooldownDate,
      expire: expireDate,
      key
    })
  }

  public check(user: string, key: string) {
    this.recycle()

    const now = new Date()

    const entries = this.getOrCreate(user).filter(
      x => isBefore(now, x.expire) && x.key === key
    )

    const hotEntry = entries.find(x => isBefore(now, x.cooldown))

    return {
      multiplier: entries.length,
      cooldown: hotEntry?.cooldown
    }
  }

  private recycle() {
    for (const [key, entries] of Object.entries(this.userMap)) {
      this.userMap[key] = entries.filter(x => isBefore(new Date(), x.expire))
    }
  }
}
