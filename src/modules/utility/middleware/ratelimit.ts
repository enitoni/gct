import { Middleware } from "@enitoni/gears-discordjs"
import { RateLimitService } from "../services/RateLimitService"
import { CommandError } from "../../core/classes/CommandError"
import { differenceInSeconds } from "date-fns"

export const ratelimit = (name = "all", seconds = 3): Middleware => (
  context,
  next
) => {
  const { manager, message } = context
  const service = manager.getService(RateLimitService)

  const { multiplier, cooldown } = service.check(message.author.id, name)
  const multipliedSeconds = (multiplier || 1) * seconds

  if (cooldown) {
    const humanizedSeconds = differenceInSeconds(cooldown, new Date()) || 1

    throw new CommandError(
      "ratelimit",
      `Please wait ${humanizedSeconds} second${
        humanizedSeconds !== 1 ? "s" : ""
      } before using this command again.`
    )
  }

  service.limit(message.author.id, name, multipliedSeconds)
  return next()
}
