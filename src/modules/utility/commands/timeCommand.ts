import { matchPrefixes } from "@enitoni/gears";
import { Command } from "@enitoni/gears-discordjs";
import { createMetadata } from "../../core/helpers/createMetadata";


export const timeCommand = new Command()
  .match(matchPrefixes("time"))
  .setMetadata(createMetadata({
    name: "Time",
    description: "Returns the current date and time of where the bot is running.",
    usage: `!time`
  }))
  .use((context) => {
    const { message } = context

    return message.channel.send(new Date().toString())
  })