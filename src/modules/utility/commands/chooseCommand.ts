import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { getRandomItem } from "../../../common/lang/array/getRandomItem"
import { createMetadata } from "../../core/helpers/createMetadata"

const responses = [
  "Hmm... Gonna go with %s.",
  "%s seems like the optimal choice.",
  "I like %s to be honest.",
  "I'm choosing %s.",
  "%s is the winner.",
  "Nothing can beat %s.",
  "My favorite is %s.",
  "Can't go wrong with %s.",
  "Honestly I think %s is the best option here.",
  "Anyone not choosing %s is a fool.",
  "Going with %s.",
  "%s. Definitely."
]

export const chooseCommand = new Command()
  .match(matchPrefixes("choose"))
  .setMetadata(
    createMetadata({
      name: "Choose",
      usage: "!choose option1|option2|option3...",
      description:
        "Let the bot choose something for you. Separate choices with |"
    })
  )
  .use(context => {
    const { content, message } = context
    const options = content.split("|")

    if (options.length === 0 || options.some(x => !x)) {
      return message.channel.send("Separate choices with |")
    }

    const choice = getRandomItem(options)
    const response = getRandomItem(responses).replace("%s", choice)

    return message.channel.send(response)
  })
