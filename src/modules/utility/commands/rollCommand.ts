import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import { createMetadata } from "../../core/helpers/createMetadata"

export const rollCommand = new Command()
  .match(matchPrefixes("d", "roll d", "roll "))
  .setMetadata(
    createMetadata({
      name: "Roll dice",
      usage: "(!d,!roll,!roll d)[number]",
      description: "Roll a number from 1 to whatever specified, default is 6."
    })
  )
  .use(context => {
    const { message, content } = context
    const limit = Number(content) || 6

    return message.channel.send(`${Math.floor(Math.random() * limit + 1)}`)
  })
