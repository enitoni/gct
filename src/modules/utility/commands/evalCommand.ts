import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { requireOwner } from "../../admin/middleware/requireOwner"
import { createMetadata } from "../../core/helpers/createMetadata"

export const evalCommand = new Command()
  .match(matchPrefixes("eval"))
  .setMetadata(
    createMetadata({
      name: "Evaluate",
      usage: "!eval",
      description: "Evaluate any JavaScript. Only the bot owner can use this."
    })
  )
  .use(requireOwner())
  .use(async context => {
    const { content, message } = context

    const result = await eval(`(async () => {${content}})()`)

    if (result === undefined) {
      return message.react("✅")
    }

    return message.channel.send(String(result), {
      code: "js"
    })
  })
