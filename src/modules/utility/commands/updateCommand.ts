import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { requireOwner } from "../../admin/middleware/requireOwner"
import axios from "axios"

export const updateCommand = new Command()
  .match(matchPrefixes("update"))
  .use(requireOwner())
  .use(async (context) => {
    const { message, bot } = context

    await message.channel.send("ok. give me a sec")
    await bot.client?.user?.setStatus("invisible")
    await axios.get(process.env.APP_UPDATE_URL!)
  })
