import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchAlways } from "@enitoni/gears"

import { rollCommand } from "../commands/rollCommand"
import { chooseCommand } from "../commands/chooseCommand"
import { evalCommand } from "../commands/evalCommand"
import { updateCommand } from "../commands/updateCommand"
import { timeCommand } from "../commands/timeCommand"

export const utilityGroup = new CommandGroup()
  .match(matchAlways())
  .setCommands(
    rollCommand,
    chooseCommand,
    evalCommand,
    updateCommand,
    timeCommand
  )
