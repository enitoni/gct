import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { HangmanService } from "../services/HangmanService"

export const startGameCommand = new Command()
  .match(matchPrefixes("new", "start", "n"))
  .use((context) => {
    const { manager, message } = context

    const service = manager.getService(HangmanService)
    const existingGame = service.getGame(message.channel)

    if (existingGame)
      return message.channel.send(
        "An existing game is already ongoing in this channel."
      )

    service.newGame(message.channel)
  })
