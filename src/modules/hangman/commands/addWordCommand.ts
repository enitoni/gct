import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { MessageEmbed } from "discord.js"
import { requireOwner } from "../../admin/middleware/requireOwner"
import { HangmanService } from "../services/HangmanService"
import { CommandError } from "../../core/classes/CommandError"

export const addWordCommand = new Command()
  .match(matchPrefixes("add-words"))
  .use(requireOwner())
  .use(async (context) => {
    const { manager, message, content } = context
    const service = manager.getService(HangmanService)

    const words = content.split("|")
    const valid = words.every((word) => /^[a-z ]+$/i.test(word))

    if (words.length === 0) {
      throw new CommandError(
        "usage",
        "You need to specify at least one word. Separate with |"
      )
    }

    if (!valid) {
      throw new CommandError(
        "usage",
        "Words can only contain alphabetical characters (A-z)"
      )
    }

    await service.addWords(words)

    const embed = new MessageEmbed()

    embed.setTitle("Words added")
    embed.setDescription(words.join(", "))
    embed.setFooter("Hangman")

    return message.channel.send({ embed })
  })
