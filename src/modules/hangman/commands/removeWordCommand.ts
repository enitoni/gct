import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import { MessageEmbed } from "discord.js"
import { requireOwner } from "../../admin/middleware/requireOwner"
import { CommandError } from "../../core/classes/CommandError"
import { HangmanService } from "../services/HangmanService"

export const removeWordCommand = new Command()
  .match(matchPrefixes("remove-words"))
  .use(requireOwner())
  .use(async (context) => {
    const { manager, message, content } = context
    const service = manager.getService(HangmanService)

    const words = content.split("|")

    if (words.length === 0) {
      throw new CommandError(
        "usage",
        "You need to specify at least one word. Separate with |"
      )
    }

    await service.removeWords(words)

    const embed = new MessageEmbed()

    embed.setTitle("Words removed")
    embed.setDescription(words.join(", "))
    embed.setFooter("Hangman")

    return message.channel.send({ embed })
  })
