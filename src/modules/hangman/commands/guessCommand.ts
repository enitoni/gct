import { Command, Middleware } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { getGameMiddleware } from "../middleware/getGameMiddleware"
import { CommandError } from "../../core/classes/CommandError"
import { ensureMember } from "../../core/middleware/ensureMember"

export const guessCommand = new Command()
  .match(matchPrefixes("guess ", "g", "letter"))
  .use(getGameMiddleware())
  .use(ensureMember())
  .use((context) => {
    const { content } = context
    const { game, member } = context.state

    if (content.length !== 1)
      throw new CommandError("usage", "Guess using a single letter")

    game.guessLetter(content, member.user)
  })
