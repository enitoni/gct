import { Command, Middleware } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { getGameMiddleware } from "../middleware/getGameMiddleware"
import { CommandError } from "../../core/classes/CommandError"
import { ensureMember } from "../../core/middleware/ensureMember"

export const guessWordCommand = new Command()
  .match(matchPrefixes("guess-word ", "gw "))
  .use(getGameMiddleware())
  .use(ensureMember())
  .use((context) => {
    const { content } = context
    const { member } = context.state

    if (!content) throw new CommandError("usage", "Specify a word")

    context.state.game.guessWord(content, member.user)
  })
