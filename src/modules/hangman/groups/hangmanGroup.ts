import { matchPrefixes } from "@enitoni/gears"
import { CommandGroup } from "@enitoni/gears-discordjs"
import { createMetadata } from "../../core/helpers/createMetadata"
import { addWordCommand } from "../commands/addWordCommand"
import { guessCommand } from "../commands/guessCommand"
import { guessWordCommand } from "../commands/guessWordCommand"
import { removeWordCommand } from "../commands/removeWordCommand"
import { startGameCommand } from "../commands/startGameCommand"

export const hangmanGroup = new CommandGroup()
  .setMetadata(
    createMetadata({
      name: "Hangman",
      usage: "!hangman,!hm <command>",
      description: "Start or interact with an ongoing hangman game."
        + "\nTo start game, the subcommand is n,new"
        + "\nTo guess a letter, the subcommand is guess,g,letter <letter>"
        + "\nTo guess an entire word, the subcommand is guess-word,gw <word>"
    })
  )
  .match(matchPrefixes("hangman", "hm"))
  .setCommands(
    addWordCommand,
    removeWordCommand,
    startGameCommand,
    guessWordCommand,
    guessCommand
  )
