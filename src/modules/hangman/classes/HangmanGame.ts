import { User } from "discord.js"
import { ScoreCounter } from "../../scoring/classes/ScoreCounter"

export type GuessResult = "correct" | "already-guessed" | "incorrect"
export type HangmanResult = "won" | "lost" | "timedOut"

export type HangmanGameOptions = {
  word: string
  health: number
  counter: ScoreCounter
  onChange: () => void
}

export class HangmanGame {
  private timer: any

  public endResult?: HangmanResult
  public guessResult?: GuessResult

  public readonly word: string
  public readonly totalHealth: number
  public health: number
  public letters: string[] = []

  public counter: ScoreCounter
  public onChange: () => void

  constructor(options: HangmanGameOptions) {
    this.word = options.word
    this.health = options.health
    this.counter = options.counter
    this.onChange = options.onChange
    this.totalHealth = options.health

    this.resetTimer()
  }

  private resetTimer() {
    clearTimeout(this.timer)

    this.timer = setTimeout(() => {
      this.endResult = "timedOut"
      this.counter.clear()
      this.onChange()
    }, 900000)
  }

  public destroy() {
    clearTimeout(this.timer)
  }

  public get displayWord() {
    return [...this.word]
      .map((char) => {
        if (char === " ") return "\u200b\u200b\u200b\u200b\u200b\u200b\u200b"

        const isGuessed = this.letters.includes(char.toLowerCase())
        return isGuessed ? char : "\\_"
      })
      .join(" ")
  }

  public get correctLetters() {
    return [...this.word].filter((char) =>
      this.letters.includes(char.toLowerCase())
    )
  }

  public guessLetter(char: string, user: User) {
    const { letters, safeWord } = this

    const safeChar = char.toLowerCase()

    /** Already guessed */
    if (letters.includes(safeChar)) {
      this.guessResult = "already-guessed"
      this.onChange()
      return
    }

    /** Correct or incorrect */
    if (safeWord.includes(safeChar)) {
      this.guessResult = "correct"
      this.counter.add(user, 15)
    } else {
      this.health -= 1
      this.guessResult = "incorrect"
    }

    this.letters.push(safeChar)

    this.resetTimer()
    this.check()
  }

  public guessWord(word: string, user: User) {
    const isCorrect = this.safeWord === this.normalizeWord(word)

    if (isCorrect) {
      this.endResult = "won"
      this.counter.add(user, 100)
      this.counter.multiply(user, this.health)
    } else {
      this.endResult = "lost"
      this.counter.clear()
    }

    this.onChange()
  }

  private check() {
    const { word, health, correctLetters } = this

    /** Guessed all letters */
    if (correctLetters.length === word.length) {
      this.endResult = "won"
    }

    /** Out of health */
    if (health < 1) {
      this.endResult = "lost"
      this.counter.clear()
    }

    this.onChange()
  }

  private normalizeWord(word: string) {
    return word.replace(/ +/g, "").toLowerCase()
  }

  private get safeWord() {
    return this.normalizeWord(this.word)
  }
}
