import { Middleware } from "@enitoni/gears-discordjs"
import { CommandError } from "../../core/classes/CommandError"
import { HangmanGame } from "../classes/HangmanGame"
import { HangmanService } from "../services/HangmanService"

export interface GameMiddlewareState {
  game: HangmanGame
}

export const getGameMiddleware = (): Middleware<GameMiddlewareState> => (
  context,
  next
) => {
  const { manager, message } = context

  const service = manager.getService(HangmanService)
  const game = service.getGame(message.channel)

  if (!game)
    throw new CommandError(
      "usage",
      "Start a game first. Use !hangman new (or !hmn)"
    )

  context.state.game = game
  return next()
}
