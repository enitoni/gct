export const CORRECT_COLOR = 0x5ced1a
export const INCORRECT_COLOR = 0xff4300
export const ALREADY_GUESSED_COLOR = 0xeaeaea
export const WIN_COLOR = 0xffc700
export const LOSE_COLOR = 0x2d2d2d

export const defaultWords = [
  "Rhythm",
  "Circle",
  "Tempo",
  "Fast",
  "Japan",
  "Norway",
  "Germany",
  "England",
]
