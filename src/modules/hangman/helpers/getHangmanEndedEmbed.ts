import { WIN_COLOR, LOSE_COLOR } from "../constants"
import { MessageEmbed } from "discord.js"
import { appendScoreboardToEmbed } from "../../scoring/helpers/appendScoreboardToEmbed"
import { HangmanResult, HangmanGame } from "../classes/HangmanGame"
import { getHangedVisual } from "./getHangedVisual"
import { getReleasedVisual } from "./getReleasedVisual"

const ticks = "```"

const titleMap: Record<HangmanResult, string> = {
  won: "Congratulations! You guessed the word!",
  lost: "Ouch. You lost. Try again another time.",
  timedOut: "The game has automatically ended because of time.",
}

const colorMap: Record<HangmanResult, number> = {
  won: WIN_COLOR,
  lost: LOSE_COLOR,
  timedOut: LOSE_COLOR,
}

export const getHangmanEndedEmbed = (game: HangmanGame) => {
  const embed = new MessageEmbed()
  const result = game.endResult!

  const visual = result === "won" ? getReleasedVisual() : getHangedVisual("💀")

  embed.setTitle(titleMap[result])
  embed.setDescription(`${ticks}\n${visual}\n${ticks}`)
  embed.setFooter("Hangman")
  embed.addField("The word was", game.word)
  embed.setColor(colorMap[result])

  appendScoreboardToEmbed(embed, game.counter.final)

  return embed
}
