const emojiStates: Record<number, string> = ["🙁", "😨", "😣", "😖", "😫"].reverse()

export const getHeadEmoji = (health: number, total: number) => {
  const percentage = health / total
  const length = Object.keys(emojiStates).length - 1

  return emojiStates[Math.floor(length * percentage)]
}
