import {
  CORRECT_COLOR,
  INCORRECT_COLOR,
  ALREADY_GUESSED_COLOR,
} from "../constants"
import { MessageEmbed } from "discord.js"
import { getHangedVisual } from "./getHangedVisual"
import { getHeadEmoji } from "./getHeadEmoji"
import { GuessResult, HangmanGame } from "../classes/HangmanGame"

const ticks = "```"

const colorMap: Record<GuessResult, number> = {
  correct: CORRECT_COLOR,
  incorrect: INCORRECT_COLOR,
  "already-guessed": ALREADY_GUESSED_COLOR,
}

export const getHangmanEmbed = (game: HangmanGame) => {
  const embed = new MessageEmbed()
  const head = getHeadEmoji(game.health, game.totalHealth)

  embed.setTitle(`Guess the word before you run out of attempts`)
  embed.setDescription(`${ticks}\n${getHangedVisual(head)}\n${ticks}`)
  embed.setFooter("Hangman")

  embed.addField("Word", game.displayWord)
  embed.addField(
    "Letters guessed",
    game.letters.join(" ") || "No letters guessed yet"
  )
  embed.addField("Attempts left", game.health)

  if (game.guessResult) {
    embed.setColor(colorMap[game.guessResult])
  }

  return embed
}
