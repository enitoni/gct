export const getReleasedVisual = () => {
  return `
  o-------o
  |       |
  |       0
  |      
  |      😌
  |      _|_
  |     / | \\
  |       |
__|__    / \\
`
}
