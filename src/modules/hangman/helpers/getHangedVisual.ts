export const getHangedVisual = (head: string) => {
  return `
  o-------o
  |       |
  |      ${head}
  |      _|_
  |     / | \\
  |       |
  |      / \\
  |
__|__
`
}
