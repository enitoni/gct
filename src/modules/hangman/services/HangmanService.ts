import { Service } from "@enitoni/gears-discordjs"
import { TextChannel } from "discord.js"
import { getRandomItem } from "../../../common/lang/array/getRandomItem"
import { JSONStorage } from "../../../common/storage/classes/JSONStorage"
import { ScoreCounter } from "../../scoring/classes/ScoreCounter"
import { HangmanGame } from "../classes/HangmanGame"
import { defaultWords } from "../constants"
import { getHangmanEmbed } from "../helpers/getHangmanEmbed"
import { getHangmanEndedEmbed } from "../helpers/getHangmanEndedEmbed"

const storage = new JSONStorage<string[]>("words.json", defaultWords)

export type HangmanChannel = Pick<TextChannel, "id" | "send">

export class HangmanService extends Service {
  private games: Record<string, HangmanGame> = {}

  public async serviceDidInitialize() {
    await storage.restore()
  }

  public getGame(channel: HangmanChannel) {
    return this.games[channel.id]
  }

  public newGame(channel: HangmanChannel) {
    if (this.games[channel.id])
      throw new Error("A game is already in progress in this channel")

    const word = getRandomItem(storage.data)
    const counter = new ScoreCounter()

    const game = (this.games[channel.id] = new HangmanGame({
      word,
      counter,
      health: 7,
      onChange: () => {
        this.handleChange(channel, game)
      },
    }))

    this.sendEmbed(channel, game)

    return game
  }

  private handleChange = (channel: HangmanChannel, game: HangmanGame) => {
    this.sendEmbed(channel, game)

    if (game.endResult) {
      game.counter.award()
      game.destroy()

      delete this.games[channel.id]
    }
  }

  private sendEmbed(channel: HangmanChannel, game: HangmanGame) {
    return channel.send({
      embed: game.endResult
        ? getHangmanEndedEmbed(game)
        : getHangmanEmbed(game),
    })
  }

  public async addWords(words: string[]) {
    const newWords = [...storage.data, ...words]
    await storage.save(newWords)
  }

  public async removeWords(words: string[]) {
    const newWords = storage.data.filter((c) => !words.includes(c))
    await storage.save(newWords)
  }
}
