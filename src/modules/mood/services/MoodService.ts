import { Service } from "@enitoni/gears-discordjs"
import { JSONStorage } from "../../../common/storage/classes/JSONStorage"
import { clamp } from "../../../common/lang/number/clamp"

export type MoodValues = {
  patience: number
  boredom: number
}

const defaultMood = {
  boredom: 0,
  patience: 1
}

const storage = new JSONStorage<MoodValues>("mood.json", defaultMood)

// 10 seconds
const TICK_RATE = 10000

export class MoodService extends Service {
  protected async serviceDidInitialize() {
    await storage.restore()

    setInterval(() => this.tick(), TICK_RATE)
  }

  private async tick() {
    return this.assign({
      boredom: this.getNewBoredom(),
      patience: this.getNewPatience()
    })
  }

  private getNewBoredom() {
    const { boredom } = storage.data
    const amount = (boredom || 0.02) / 5

    return clamp(boredom + amount)
  }

  private getNewPatience() {
    const { patience } = storage.data

    return clamp(patience + 0.05)
  }

  private assign(values: Partial<MoodValues>) {
    return storage.save({ ...storage.data, ...values })
  }

  public reset() {
    return this.assign(defaultMood)
  }

  public get data() {
    return storage.data
  }
}
