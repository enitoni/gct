import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { requireOwner } from "../../admin/middleware/requireOwner"
import { MoodService } from "../services/MoodService"

export const resetMoodCommand = new Command()
  .match(matchPrefixes("reset"))
  .use(requireOwner())
  .use(async context => {
    const { manager, message } = context
    const service = manager.getService(MoodService)

    await service.reset()

    return message.channel.send("I feel alright now.")
  })
