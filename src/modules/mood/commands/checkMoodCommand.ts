import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes, matchAlways } from "@enitoni/gears"
import { MoodService } from "../services/MoodService"
import { MessageEmbed } from "discord.js"
import { capitalize } from "../../../common/lang/string/capitalize"

export const checkMoodCommand = new Command()
  .match(matchAlways())
  .use((context) => {
    const { message, manager } = context
    const service = manager.getService(MoodService)

    const fields = Object.entries(service.data)
      .map(([key, value]) => `${capitalize(key)}: ${Math.floor(value) * 100}%`)
      .join("\n")

    const embed = new MessageEmbed({
      title: "Mood",
      description: fields,
    })

    return message.channel.send(embed)
  })
