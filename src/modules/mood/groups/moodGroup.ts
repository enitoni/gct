import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { checkMoodCommand } from "../commands/checkMoodCommand"
import { resetMoodCommand } from "../commands/resetMoodCommand"

export const moodGroup = new CommandGroup()
  .match(matchPrefixes("mood", "m"))
  .setCommands(resetMoodCommand, checkMoodCommand)
