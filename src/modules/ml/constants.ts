
export const SENTENCE_SENTIMENTS = ["negative", "neutral", "positive"] as const
export const SENTENCE_TYPES = ["direct-statement", "indirect-statement", "open", "closed", "rating"] as const
export const SENTENCE_ENTROPY = SENTENCE_SENTIMENTS.length * SENTENCE_TYPES.length

export const TARGET_DATA_SIZE = SENTENCE_ENTROPY * 1000

export const DISCARD_DATA_SESSION_KEYWORD = "q;"