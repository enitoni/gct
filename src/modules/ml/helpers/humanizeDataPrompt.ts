import { Combination } from "../classes/DataSession";
import { DISCARD_DATA_SESSION_KEYWORD } from "../constants";
import { SentenceType } from "../types";

const typeToStringMap: Record<SentenceType, string> = {
    "direct-statement": "statement directed at the listener",
    "indirect-statement": "statement not directed at the listener",
    closed: "closed ended question",
    open: "open ended question",
    rating: "rating request"
}

export const humanizeDataPrompt = (combination: Combination, iteration: number, maxIterations: number) => {
    return `Write a ${combination.sentiment} ${typeToStringMap[combination.type]}. (${iteration} of ${maxIterations}, ${DISCARD_DATA_SESSION_KEYWORD} to discard/quit)`
}