import { Matcher } from "@enitoni/gears-discordjs";
import { TextChannel } from "discord.js";
import { DataSessionService } from "../services/DataSessionService";

export const matchSession = (): Matcher => (context) => {
    const { manager, message } = context

    const service = manager.getService(DataSessionService)
    
    if (service.hasSession(message.channel as TextChannel, message.author)) {
        return context
    }
}