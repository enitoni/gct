import { matchAll, matchPrefixes } from "@enitoni/gears";
import { CommandGroup } from "@enitoni/gears-discordjs";
import { matchContributors } from "../../admin/matchers/matchContributors";
import { createSessionCommand } from "../commands/createSessionCommand";

export const mlGroup =  new CommandGroup()
    .match(matchAll(matchPrefixes("ml"), matchContributors()))
    .setCommands(createSessionCommand)