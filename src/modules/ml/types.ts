import { SENTENCE_SENTIMENTS, SENTENCE_TYPES } from "./constants"

export type Sentiment = (typeof SENTENCE_SENTIMENTS)[number]
export type SentenceType = (typeof SENTENCE_TYPES)[number]

export type SentenceData = {
    sentiment: Sentiment,
    type: SentenceType,
    content: string,
}