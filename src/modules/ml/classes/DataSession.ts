import { TextChannel, User } from "discord.js";
import { SENTENCE_ENTROPY, SENTENCE_SENTIMENTS, SENTENCE_TYPES } from "../constants";
import { SentenceData } from "../types";

export type DataSessionOptions = {
    iterations: number,
    channel: TextChannel,
    user: User,
}

export type Combination = Omit<SentenceData, "content">


// Represents a session of a user giving the bot training data
export class DataSession {
    constructor(private options: DataSessionOptions) {}
    public data: SentenceData[] = []

    public submit(content: string) {
        const { sentiment, type } = this.combination
        const item = { content, sentiment, type}

        this.data.push(item)
    }

    public get combination(): Combination {
        const { length } = this.data

        const sentiment = SENTENCE_SENTIMENTS[length % SENTENCE_SENTIMENTS.length]
        const type = SENTENCE_TYPES[Math.floor(length / SENTENCE_SENTIMENTS.length)]

        return { sentiment, type }
    }

    public get iteration() {
        return this.data.length
    }

    public get maxIterations() {
        return this.options.iterations * SENTENCE_ENTROPY
    }

    public get finished() {
        return this.iteration >= this.maxIterations
    }

    public get channel() {
        return this.options.channel
    }

    public get user() {
        return this.options.user
    }
}  