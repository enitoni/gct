import { Command } from "@enitoni/gears-discordjs";
import { TextChannel } from "discord.js";
import { DISCARD_DATA_SESSION_KEYWORD } from "../constants";
import { humanizeDataPrompt } from "../helpers/humanizeDataPrompt";
import { matchSession } from "../matchers/matchSession";
import { DataSessionService } from "../services/DataSessionService";

export const submitDataCommand = new Command()
    .match(matchSession())
    .use(context => {
        const { message, content, manager } = context

        const service = manager.getService(DataSessionService)

        if (content.trim() === DISCARD_DATA_SESSION_KEYWORD) {
            service.discardSession(message.channel as TextChannel, message.author)

            return message.channel.send("Data discarded.")
        }

        const { combination, iteration, maxIterations, finished } = service.submit(message.channel as TextChannel, message.author, content)
        
        if (!finished) {
            return message.channel.send(humanizeDataPrompt(combination, iteration, maxIterations))
        }

        const { current, target } = service.stats
        return message.channel.send(`✅ New data submitted. (${current} out of ${target})`)
    })