import { matchAll, matchNone, matchPrefixes } from "@enitoni/gears";
import { Command } from "@enitoni/gears-discordjs";
import { TextChannel } from "discord.js";
import { humanizeDataPrompt } from "../helpers/humanizeDataPrompt";
import { matchSession } from "../matchers/matchSession";
import { DataSessionService } from "../services/DataSessionService";

const matchNotSession = matchNone(matchSession())

export const createSessionCommand = new Command()
    .match(matchAll(matchPrefixes("add-data", "add"), matchNotSession))
    .use(context => {
        const { manager, message, content } = context

        const service = manager.getService(DataSessionService)
        const { combination, iteration, maxIterations } = service.createSession(message.channel as TextChannel, message.author, Number(content || "1"))

        return message.channel.send(humanizeDataPrompt(combination, iteration, maxIterations))
    })