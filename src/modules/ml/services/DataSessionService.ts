import { Service } from "@enitoni/gears-discordjs";
import { TextChannel, User } from "discord.js";
import { JSONStorage } from "../../../common/storage/classes/JSONStorage";
import { DataSession } from "../classes/DataSession";
import { TARGET_DATA_SIZE } from "../constants";
import { SentenceData } from "../types";

const storage = new JSONStorage<SentenceData[]>("ml-sentence-dataset.json", [])
const regex = new RegExp("^[ a-z0-9]")

export class DataSessionService extends Service {
    private sessions: DataSession[] = []

    public async serviceDidInitialize() {
        await storage.restore()
    }

    private getSession(channel: TextChannel, user: User) {
        return this.sessions.find(s => s.channel.id === channel.id && s.user.id === user.id)
    }

    private consumeSession(session: DataSession) {
        this.sessions = this.sessions.filter(s => s !== s)
        storage.save([...storage.data, ...session.data])
    }

    public discardSession(channel: TextChannel, user: User) {
        this.sessions = this.sessions.filter(s => s !== this.getSession(channel, user)!)
    }

    public hasSession(channel: TextChannel, user: User) {
        return !!this.getSession(channel, user)
    }

    public createSession(channel: TextChannel, user: User, iterations = 1) {
        const newSession = new DataSession({ user, channel, iterations })

        this.sessions.push(newSession)
        return this.getSessionStatus(channel, user)
    }

    public submit(channel: TextChannel, user: User, content: string) {
        const session = this.getSession(channel, user)!

        const safeContent = [...content.toLowerCase().trim()].filter(x => regex.test(x)).join("")

        if (safeContent.length === 0) {
            throw new Error("Content length cannot be 0")
        }

        session.submit(safeContent)
        const status = this.getSessionStatus(channel, user)

        if (session.finished) {
            this.consumeSession(session)
        }

        return status
    }

    public getSessionStatus(channel: TextChannel, user: User) {
        const { finished, iteration, maxIterations, combination } = this.getSession(channel, user)!
        return { finished, iteration, maxIterations, combination }
    }

    public get stats() {
        return {
            target: TARGET_DATA_SIZE,
            current: storage.data.length,
        }
    }
}