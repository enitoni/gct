import { MessageEmbed } from "discord.js"
import { getSortedScores } from "./getSortedScores"

export const appendScoreboardToEmbed = (
  embed: MessageEmbed,
  scores: Record<string, number>
) => {
  const sortedScores = getSortedScores(scores)

  const scoreString =
    sortedScores
      .map(({ user, score }, i) => `${i + 1}. <@${user}> - ${score}`)
      .join("\n") || "No scores yet"

  if (!scoreString) return

  embed.addField("Scores", scoreString, true)

  return
}
