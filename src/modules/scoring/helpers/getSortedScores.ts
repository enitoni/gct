export const getSortedScores = (scores: Record<string, number>, limit = 5) => {
  return Object.entries(scores)
    .sort(([, a], [, b]) => (a > b ? -1 : 1))
    .map(([user, score]) => ({ user, score }))
    .slice(0, limit)
}
