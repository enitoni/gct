import { User } from "discord.js"

export class ScoreCounter {
  private consumed = false

  private scores: Record<string, number> = {}

  constructor(public initial = 0) {}

  private ensureNotConsumed() {
    if (this.consumed) {
      throw new Error(
        "Cannot modify ScoreCounter that has already been awarded"
      )
    }
  }

  private getSafeScore(user: User) {
    return this.scores[user.id] || 0
  }

  public add(user: User, points: number) {
    this.ensureNotConsumed()
    this.scores[user.id] = this.getSafeScore(user) + points
  }

  public multiply(user: User, rounds: number) {
    this.ensureNotConsumed()
    this.scores[user.id] = this.getSafeScore(user) * rounds
  }

  public clear() {
    for (const [user] of Object.entries(this.scores)) {
      this.scores[user] = 0
      this.initial = 0
    }
  }

  public setInitial(score: number) {
    this.initial = score > 0 ? score : 0
  }

  public get final() {
    const scores = this.scores

    for (const [key, value] of Object.entries(scores)) {
      scores[key] = Math.floor(value + this.initial)
    }

    return scores
  }

  public async award() {
    this.ensureNotConsumed()
    this.consumed = true

    // await this.service.assignScores(this.final)
  }
}
