import { Middleware } from "@enitoni/gears-discordjs"
import { CommandError } from "../classes/CommandError"
import { createErrorEmbed } from "../helpers/createErrorEmbed"

export const handleError = (): Middleware => async (context, next) => {
  const { message } = context

  try {
    return await next()
  } catch (error) {
    message.channel.stopTyping(true)

    console.error(error)

    if (error instanceof CommandError) {
      const embed = createErrorEmbed(error.type, error.reason)
      return message.channel.send(embed)
    }

    const embed = createErrorEmbed("unknown", error.message)
    return message.channel.send(embed)
  }
}
