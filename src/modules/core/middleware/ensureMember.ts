import { Middleware } from "@enitoni/gears-discordjs"
import { GuildMember } from "discord.js"
import { CommandError } from "../classes/CommandError"

type State = {
  member: GuildMember
}

export const ensureMember = (): Middleware<State> => (context, next) => {
  const { message } = context

  if (!message.member) {
    throw new CommandError(
      "usage",
      "This command can only be used in a server."
    )
  }

  context.state.member = message.member
  return next()
}
