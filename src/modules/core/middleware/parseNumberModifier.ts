import { Middleware } from "@enitoni/gears-discordjs"
import { clamp } from "../../../common/lang/number/clamp"

export type State = {
  parsedNumber: {
    value: number
    strippedContent: string
  }
}

export const parseNumberModifier = (
  min = 0,
  max = Infinity
): Middleware<State> => (context, next) => {
  const { content, state } = context
  const match = /^:(\d+)(?:\s|$)/.exec(content)

  const value = clamp(match ? Number(match[1]) : min, max, min)
  const safeContent = match ? content.replace(match[0], "") : content

  state.parsedNumber = {
    strippedContent: safeContent,
    value,
  }

  return next()
}
