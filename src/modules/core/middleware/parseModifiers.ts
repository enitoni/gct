import { Middleware } from "@enitoni/gears-discordjs"
import { clamp } from "../../../common/lang/number/clamp"
import { CommandError } from "../classes/CommandError"

export type Parser<T> = (label: string, value: string) => T

export type Modifier<T> = {
  name: string
  label: string
  parse: Parser<T>
  default: T
}

export type State<T extends readonly Modifier<unknown>[]> = {
  modifiers: {
    [M in T[number] as M["name"]]: ReturnType<M["parse"]>
  }
}

export const parseModifiers = <T extends Modifier<unknown>[]>(
  modifiers: T
): Middleware<State<T>> => (context, next) => {
  const { content, state } = context

  const result = Object.fromEntries(
    modifiers.map((x) => [x.name, x.default])
  ) as typeof state["modifiers"]
  state.modifiers = result

  if (!content.startsWith(".")) {
    return next()
  }

  const [argString, strippedContent] = /(\S+)(?:\s(.*))?/s
    .exec(content)
    ?.slice(1) || ["", ""]

  const args = argString.split(".").slice(1, modifiers.length + 1)

  context.content = strippedContent

  for (const [i, modifier] of modifiers.slice(0, args.length).entries()) {
    ;(result as any)[modifier.name] = modifier.parse(modifier.label, args[i])
  }

  return next()
}

// Default parsers
export const parseNumber = (min = 0, max = Infinity): Parser<number> => (
  label,
  v
) => {
  const number = Number(v)

  if (Number.isNaN(number) || number < min || number > max) {
    throw new CommandError(
      "usage",
      `${label} must be a number between ${min} and ${max}.`
    )
  }

  return number
}
