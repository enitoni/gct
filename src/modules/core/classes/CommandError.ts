export type CommandErrorType = "usage" | "permission" | "unknown" | "ratelimit"

export class CommandError extends Error {
  constructor(public type: CommandErrorType, public reason?: string) {
    super()
  }
}
