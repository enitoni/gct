import { CommandErrorType } from "../classes/CommandError"
import { MessageEmbed } from "discord.js"

const titleMap: Record<CommandErrorType, string> = {
  ratelimit: "You're doing that too much",
  permission: "You don't have permission",
  unknown: "An unknown error occurred",
  usage: "Usage error",
}

export const createErrorEmbed = (type: CommandErrorType, message?: string) => {
  return new MessageEmbed({
    title: titleMap[type],
    description: message,
    color: 0xf55742,
  })
}
