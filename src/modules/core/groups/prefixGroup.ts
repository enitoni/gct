import { CommandGroup } from "@enitoni/gears-discordjs"

import { funGroup } from "../../fun/groups/funGroup"
import { utilityGroup } from "../../utility/groups/utilityGroup"
import { helpCommand } from "../commands/helpCommand"
import { serverTranslateGroup } from "../../translate/groups/serverTranslateGroup"
import { translateGroup } from "../../translate/groups/translateGroup"
import { moodGroup } from "../../mood/groups/moodGroup"
import { c19Group } from "../../c19/group/c19Group"
import { adminGroup } from "../../admin/group/adminGroup"
import { matchCustomPrefix } from "../../admin/matchers/matchCustomPrefix"
import { hangmanGroup } from "../../hangman/groups/hangmanGroup"
import { base64Group } from "../../fun/groups/base64Group"
import { mlGroup } from "../../ml/groups/mlGroup"

export const prefixGroup = new CommandGroup()
  .match(matchCustomPrefix)
  .setCommands(
    mlGroup,
    c19Group,
    funGroup,
    moodGroup,
    adminGroup,
    base64Group,
    utilityGroup,
    hangmanGroup,
    translateGroup,
    serverTranslateGroup,
    helpCommand
  )
