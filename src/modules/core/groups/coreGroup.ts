import { CommandGroup } from "@enitoni/gears-discordjs"
import { prefixGroup } from "./prefixGroup"
import { okCommand } from "../../fun/commands/okCommand"
import { handleError } from "../middleware/handleError"
import { matchNotIgnored } from "../../admin/matchers/matchNotIgnored"
import { submitDataCommand } from "../../ml/commands/submitDataCommand"

export const coreGroup = new CommandGroup()
  .match(matchNotIgnored())
  .use(handleError())
  .setCommands(submitDataCommand, prefixGroup, okCommand)