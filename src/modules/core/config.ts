import * as path from "path"
import * as fs from "fs"

export type Config = {
  token: string
  microsoft: {
    key: string
    endpoint: string
    region: string
  }
}

export const config: Config = JSON.parse(
  fs.readFileSync(
    path.resolve(process.env.APP_CONFIG || process.cwd(), "config.json"),
    "utf8"
  )
)
