import { Command } from "@enitoni/gears-discordjs"
import { createMetadata } from "../helpers/createMetadata"
import { matchPrefixes } from "@enitoni/gears"
import { mapTreeToMetadata } from "../helpers/mapTreeToMetadata"
import { MessageEmbed } from "discord.js"
import { getPrefixIdentifier } from "../../admin/helpers/getPrefixIdentifier"
import { PrefixService } from "../../admin/services/PrefixService"

export const helpCommand = new Command()
  .match(matchPrefixes("help"))
  .setMetadata(
    createMetadata({
      name: "help",
      usage: "!help",
      description: "Shows this output",
    })
  )
  .use((context) => {
    const { bot, message, manager } = context
    const metadata = mapTreeToMetadata(bot.group)

    const service = manager.getService(PrefixService)
    const [prefix] = service.getPrefix(getPrefixIdentifier(message))

    const embed = new MessageEmbed({
      title: "Available commands",
      description: "Here's a list of commands.",
      fields: metadata.map((x) => ({
        name: "`" + x.usage.replace("!", prefix) + "`",
        value: `${x.description}\n \n`,
      })),
    })

    return message.channel.send(embed)
  })
