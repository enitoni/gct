import { Context } from "@enitoni/gears-discordjs"
import { Message } from "discord.js"

export const temporaryCreateReplyWorkaround = async (
  context: Context,
  content: string,
  message: Message
): Promise<Message> => {
  // @ts-ignore
  const result = await context.bot.client.api.channels[
    message.channel.id
  ].messages.post({
    data: {
      content,
      message_reference: { message_id: message.id, fail_if_not_exists: false },
      allowed_mentions: { parse: [], replied_user: false },
    },
  })

  // @ts-ignore
  return context.bot.client.actions.MessageCreate.handle(result).message
}
