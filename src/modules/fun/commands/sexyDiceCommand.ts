import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { getRandomItem } from "../../../common/lang/array/getRandomItem"
import { createMetadata } from "../../core/helpers/createMetadata"
import { ratelimit } from "../../utility/middleware/ratelimit"

const actions = [
  "lick",
  "kick",
  "grab",
  "eat",
  "spank",
  "fuck",
  "stroke",
  "suck",
  "blow",
  "sniff",
  "twist",
  "torture",
  "pull",
  "whip",
  "sit on",
  "step on",
  "whisper into",
  "milk",
  "hold",
  "tie up"
]

const bodyparts = [
  "eyes",
  "stomach",
  "lips",
  "ears",
  "eyes",
  "ass",
  "toes",
  "leg",
  "arms",
  "armpits",
  "fingers",
  "neck",
  "hair",
  "cock",
  "nipples",
  "balls",
  "nose",
  "feet",
  "thighs"
]

export const sexyDiceCommand = new Command()
  .match(matchPrefixes("roll-sexy-dice", "rsd"))
  .setMetadata(
    createMetadata({
      name: "Roll sexy dice",
      usage: "!rsd",
      description:
        "Returns a random combination of an erotic action and a body part. Inspired by the sex dice from The Simpsons."
    })
  )
  .use(ratelimit())
  .use(context => {
    const { message } = context

    const action = getRandomItem(actions)
    const bodypart = getRandomItem(bodyparts)

    return message.channel.send(`${action} ${bodypart}`)
  })
