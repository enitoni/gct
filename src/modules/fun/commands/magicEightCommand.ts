import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { getRandomItem } from "../../../common/lang/array/getRandomItem"
import { getHashFromString } from "../../../common/lang/string/getHashFromString"
import { createMetadata } from "../../core/helpers/createMetadata"

const responses = [
  "It is certain.",
  "Maybe.",
  "It is decidedly so.",
  "Without a doubt.",
  "You may rely on it.",
  "As I see it, yes.",
  "Most likely.",
  "Outlook good.",
  "Yes.",
  "Signs point to yes.",
  "Reply hazy, try again.",
  "Ask again later.",
  "Better not tell you now.",
  "Cannot predict now.",
  "Concentrate and ask again.",
  "Don’t count on it.",
  "My reply is no.",
  "My sources say no.",
  "Outlook not so good.",
  "Very doubtful."
]

const positiveResponses = [
  "It is certain.",
  "It is decidedly so.",
  "Without a doubt.",
  "You may rely on it.",
  "As I see it, yes.",
  "Most likely.",
  "Outlook good.",
  "Yes.",
  "Signs point to yes."
]

const negativeResponses = [
  "Don’t count on it.",
  "My reply is no.",
  "My sources say no.",
  "Outlook not so good.",
  "Very doubtful."
]

const neutralResponses = [
  "Reply hazy, try again.",
  "Ask again later.",
  "Better not tell you now.",
  "Cannot predict now.",
  "Concentrate and ask again."
]

const notQuestionResponses = ["Please ask a question.", "..."]
const responseGroup = [positiveResponses, negativeResponses]

export const magicEightCommand = new Command()
  .match(matchPrefixes("8", "magic"))
  .setMetadata(
    createMetadata({
      name: "Magic 8 ball",
      usage: "!8,!magic <phrase>",
      description: "Ask the magic 8 ball anything and get an answer."
    })
  )
  .use(context => {
    const { message, content } = context
    const { channel, author } = message

    if (!content) {
      return channel.send(getRandomItem(notQuestionResponses))
    }

    if (Math.random() > 0.85) {
      return channel.send(getRandomItem(neutralResponses))
    }

    const sanitizedContent = content
      .trim()
      .toLowerCase()
      .replace(/\W/g, "")

    const user = message.mentions.users.first() || message.author

    const hash = Math.abs(getHashFromString(`${sanitizedContent}-${user.id}`))
    const responses = responseGroup[hash % responseGroup.length]

    return channel.send(getRandomItem(responses))
  })
