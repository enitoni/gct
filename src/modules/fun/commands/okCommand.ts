import { Command } from "@enitoni/gears-discordjs"
import { createMetadata } from "../../core/helpers/createMetadata"

const phrases = [
  `"why didn't you just listen to the police"`,
  "greta thunberg is a victim of child abuse",
  "electronic music is not real music",
  "brexit is the will of the people",
  "videogames cause violence",
  "make america great again",
  "climate change is a hoax",
  "cellphones cause cancer",
  "it's okay to be white",
  "pause fortnite please",
  "i read it on facebook",
  "vaccines cause autism",
  "brexit means brexit",
  "proudly uneducated",
  "abortion is murder",
  "blue lives matter",
  "obama is a muslim",
  "get a real job",
  "cheese pizza",
  "5g causes",
  "ok retard",
]

export const okCommand = new Command()
  .setMetadata(
    createMetadata({
      name: "OK Boomer",
      usage: "ok boomer (passive)",
      description: `Replies with "ok boomer" for certain phrases. Up to you to figure out which.`,
    })
  )
  .match((context) => {
    const { message } = context

    for (const phrase of phrases) {
      if (message.content.toLowerCase().indexOf(phrase) >= 0) {
        return context
      }
    }

    return undefined
  })
  .use((context) => {
    return context.message.channel.send("ok boomer")
  })
