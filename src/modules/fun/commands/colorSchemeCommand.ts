import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"
import { createMetadata } from "../../core/helpers/createMetadata"
import { parseNumberModifier } from "../../core/middleware/parseNumberModifier"

const min = 2
const max = 10

export const colorSchemeCommand = new Command()
  .match(matchPrefixes("rc"))
  .setMetadata(
    createMetadata({
      name: "Random color scheme",
      usage: `!rc[:${min}-${max}]`,
      description: `Generates random colors with coolors.co. Default count is ${min}.`,
    })
  )
  .use(parseNumberModifier(min, max))
  .use((context) => {
    const { message, state } = context
    const { value } = state.parsedNumber

    const colors = new Array(value).fill(undefined).map((x) =>
      Math.floor(Math.random() * 0x1000000)
        .toString(16)
        .padStart(6, "0")
    )

    return message.channel.send(`https://coolors.co/${colors.join("-")}`)
  })
