import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { createMetadata } from "../../core/helpers/createMetadata"

export const spongebobCommand = new Command()
  .match(matchPrefixes("sb", "spongebob"))
  .setMetadata(
    createMetadata({
      name: "Spongebob format",
      usage: "!sb <phrase>",
      description:
        "Converts a phrase to random uppercase and lowercase much like the spongebob meme."
    })
  )
  .use(context => {
    const { content, message } = context

    let counter = 0
    let lastWasUpperCase = false

    const result = content
      .split("")
      .map(c => {
        let upperCase = Math.random() > 0.5

        if (upperCase === lastWasUpperCase) {
          counter++

          if (counter > 4) {
            counter = 0
            upperCase = !upperCase
          }
        } else {
          counter = 0
        }

        lastWasUpperCase = upperCase
        return upperCase ? c.toUpperCase() : c.toLowerCase()
      })
      .join("")

    return message.channel.send(result)
  })
