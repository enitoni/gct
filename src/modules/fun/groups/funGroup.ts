import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchAlways } from "@enitoni/gears"
import { sexyDiceCommand } from "../commands/sexyDiceCommand"
import { magicEightCommand } from "../commands/magicEightCommand"
import { spongebobCommand } from "../commands/spongebobCommand"
import { colorSchemeCommand } from "../commands/colorSchemeCommand"

export const funGroup = new CommandGroup()
  .match(matchAlways())
  .setCommands(
    sexyDiceCommand,
    magicEightCommand,
    spongebobCommand,
    colorSchemeCommand
  )
