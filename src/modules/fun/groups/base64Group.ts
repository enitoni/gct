import { matchAlways, matchPrefixes } from "@enitoni/gears"
import { Command, CommandGroup } from "@enitoni/gears-discordjs"
import { createMetadata } from "../../core/helpers/createMetadata"

const encode = new Command().match(matchPrefixes("atob")).use((context) => {
  const { content, message } = context
  return message.channel.send(Buffer.from(content).toString("base64"))
})

const decode = new Command().match(matchPrefixes("btoa")).use((context) => {
  const { content, message } = context
  return message.channel.send(Buffer.from(content, "base64").toString())
})

export const base64Group = new CommandGroup()
  .setMetadata(
    createMetadata({
      name: "base64",
      description: "Encode or decode base64",
      usage: "!(atob, btoa) <text>",
    })
  )
  .match(matchAlways())
  .setCommands(encode, decode)
