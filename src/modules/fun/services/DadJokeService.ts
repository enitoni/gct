import { Service } from "@enitoni/gears-discordjs"
import { Message } from "discord.js"
import { getRandomItem } from "../../../common/lang/array/getRandomItem"

const insults = [
  "if you're gonna make dad jokes, make like my dad and leave",
  "were you born on the highway? that's where accidents happen",
  "when i grow up i wanna be as funny as you think you are",
  "i'd prefer a fart over your asshole",
  "i was pro life until i met you",
  "go be a jerk to your parents",
  "go sit in a corner",
  "take your meds",
  "haha so funny",
  "shut up",
  "lol ok",
  "hi dad",
  "stop",
  "who?",
]

export class DadJokeService extends Service {
  private potential = ""

  public serviceDidInitialize() {
    const { bot } = this

    bot.client.on("message", this.handleMessage)
  }

  private handleMessage = (message: Message) => {
    const potentialMatch = /i(?:'| a)?m\s+([\s\S]+)/i.exec(message.content)

    if (potentialMatch) {
      const [, potential] = potentialMatch
      this.potential = potential

      console.log(potential)

      return
    }

    const answerMatch = /(?:hi|hello)\s+([\s\S]+)/i.exec(message.content)
    if (!answerMatch) return

    const [, answer] = answerMatch

    if (this.potential && answer.includes(this.potential)) {
      message.channel.send(getRandomItem(insults))
      this.potential = ""
    }
  }
}
